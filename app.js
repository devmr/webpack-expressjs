const express = require('express')
const app = express()
const port = 3000

// From webpack.config.js:
app.use('/static', express.static('public'));

// set the view engine to ejs
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.render('pages/index');
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})